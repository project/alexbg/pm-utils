﻿using DevExpress.Compression;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace NugetExplorer {
    public static class XmlReaderExt {
        public static XmlReader ReadToElement(this XmlReader reader) {
            while (reader.Read())
                if (reader.NodeType == XmlNodeType.Element)
                    break;
            return reader;
        }
        public static XmlReader Skip(this XmlReader reader, params string[] names) {
            while (names.Contains(reader.Name))
                reader.ReadToElement();
            return reader;
        }
        public static XmlReader SkipUntilOrThrow(this XmlReader reader, string name) {
            while (reader.Name != name)
                reader.ReadToElement();
            return reader.ThrowIfNameInvalid(name);
        }
        public static XmlReader ThrowIfNameInvalid(this XmlReader reader, params string[] names) {
            if (names.Contains(reader.Name))
                return reader;
            throw new InvalidOperationException();
        }
    }

    class NugetSpec {
        HashSet<string> dependencies;
        
        IEnumerable<string> ParseDependencyGroup(XmlReader reader) {
            reader.ThrowIfNameInvalid("group");
            while (true) {
                reader.ReadToElement();
                if (reader.Name != "dependency")
                    break;
                yield return reader.GetAttribute("id");
            }
        }

        public string ID { get; set; }
        public ICollection<string> Dependencies => dependencies ?? (dependencies = new HashSet<string>());

        public NugetSpec(Stream stream) {
            using (var reader = XmlReader.Create(stream)) {
                reader.ReadToElement().ThrowIfNameInvalid("package");
                reader.ReadToElement().ThrowIfNameInvalid("metadata");
                reader.ReadToElement().ThrowIfNameInvalid("id");
                ID = reader.ReadElementContentAsString();

                reader.SkipUntilOrThrow("dependencies");
                reader.ReadToElement().ThrowIfNameInvalid("group");
                while (reader.Name == "group") {
                    foreach (var id in ParseDependencyGroup(reader))
                        Dependencies.Add(id);
                }
            }
        }
        public override string ToString() {
            return ID;
        }
    }

    class NugetPackage: IDisposable {
        public static IEnumerable<NugetPackage> LoadPackages(string folder) {
            var files = Directory.GetFiles(folder, "*.nupkg");
            foreach (var filePath in files)
                yield return new NugetPackage(filePath);
        }
        
        ZipArchive zipPackage;

        NugetPackage(string path) {
            this.zipPackage = ZipArchive.Read(path);
        }
        public NugetSpec GetNugetSpec() {
            var zipItem = zipPackage.First(x => x.Name.EndsWith(".nuspec"));
            using (var stream = zipItem.Open()) {
                return new NugetSpec(stream);
            }
        }
        public void Dispose() {
            zipPackage.Dispose();
        }
    }

    class Nugets {
        static string lastNugetFolder;
        static Dictionary<string, NugetSpec> nuSpecsCache;

        public static IEnumerable<NugetSpec> FindNuSpecs(string[] patterns, string nuggetsFolder) {
            InitNuSpecCache(nuggetsFolder);

            foreach (var str in patterns)
            foreach (var pair in nuSpecsCache) {
                var nuspec = pair.Value;
                if (nuspec.ID.Contains(str))
                    yield return nuspec;
            }
        }
        public static DependencyHierarchy BuildHierarchy(string dependencyName, string nuggetsFolder) {
            InitNuSpecCache(nuggetsFolder);
            return new DependencyHierarchy(dependencyName, nuSpecsCache);
        }
        static void InitNuSpecCache(string nuggetsFolder) {
            if (nuSpecsCache != null && lastNugetFolder == nuggetsFolder)
                return;

            nuSpecsCache = new Dictionary<string, NugetSpec>();
            foreach (var package in NugetPackage.LoadPackages(nuggetsFolder)) {
                var nuspec = package.GetNugetSpec();
                nuSpecsCache.Add(nuspec.ID, nuspec);
            }
            lastNugetFolder = nuggetsFolder;
        }
    }

    class DependencyHierarchy {
        public readonly string ID;
        Dictionary<string, NugetSpec> nuSpecsCache;

        IList<DependencyHierarchy> referencedBy;
        public IList<DependencyHierarchy> ReferencedBy => referencedBy ?? (referencedBy = BuildReferencedBy().ToList());

        public DependencyHierarchy(string dependencyName, Dictionary<string, NugetSpec> nuSpecsCache) {
            ID = dependencyName;
            this.nuSpecsCache = nuSpecsCache;
        }
        public override string ToString() {
            return $"{ID} : {ReferencedBy.Count}";
        }
        IEnumerable<DependencyHierarchy> BuildReferencedBy() {
            foreach (var pair in nuSpecsCache) {
                var nuspec = pair.Value;
                if (nuspec.ID == ID)
                    continue;
                if (nuspec.Dependencies.Contains(ID))
                    yield return new DependencyHierarchy(nuspec.ID, nuSpecsCache);
            }
        }
    }
}