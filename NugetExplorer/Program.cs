﻿using System;
using System.Linq;

namespace NugetExplorer {
    class Program {
        static void OutputReferencedBy(DependencyHierarchy hierarсhy, int indent = 0, int nestingLevel = 2) {
            if (nestingLevel <= 0)
                return;
            Console.WriteLine(new string(' ', indent) + hierarсhy.ID);
            foreach (var referencedBy in hierarсhy.ReferencedBy)
                OutputReferencedBy(referencedBy, indent + 4, nestingLevel - 1);
        }

        static void Main(string[] args) {
            var nugetsFolder = args[0];

            //var patterns = new []{ "Rich", "Spreadsheet", "PdfViewer", "Snap", "SpellChecker", "Docs", "Processor"};
            //var specs = Nugets.FindNuSpecs(patterns, nuggetsFolder).ToList();
            //specs.ForEach(x => Console.WriteLine(x));

            var packages = new[] {
                "DevExpress.RichEdit.Core",
                "DevExpress.RichEdit.Export",
                "DevExpress.Win.RichEdit",
                "DevExpress.Wpf.RichEdit",
                "DevExpress.Spreadsheet.Core",
                "DevExpress.Win.Spreadsheet",
                "DevExpress.Wpf.Spreadsheet",
                "DevExpress.Win.PdfViewer",
                "DevExpress.Wpf.PdfViewer",
                "DevExpress.Snap.Core",
                "DevExpress.Win.Snap",
                "DevExpress.SpellChecker.Core",
                "DevExpress.Win.SpellChecker",
                "DevExpress.Wpf.SpellChecker",
                "DevExpress.Document.Processor",
                "DevExpress.Utils.UI",
                "DevExpress.Win.Demos"
            };

            foreach(var packageId in packages) { 
                var hierarсhy = Nugets.BuildHierarchy(packageId, nugetsFolder);
                OutputReferencedBy(hierarсhy);
            };
        }
    }
}
