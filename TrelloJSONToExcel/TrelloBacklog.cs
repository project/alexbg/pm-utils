﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace TrelloJSONToExcel {
#pragma warning disable 0169, 0649
    [JsonObject(MemberSerialization.Fields)]
    public class TrelloCard {
        [JsonProperty("id")] public readonly string ID;
        [JsonProperty("name")] public readonly string Name;
        [JsonProperty("idList")] public readonly string ListID;
        //[JsonProperty("shortUrl")] public readonly string ShortUrl;
        [JsonProperty("url")] public readonly string Url;
        [JsonProperty("shortLink")] public readonly string ShortLink;
        [JsonProperty("dateLastActivity")] public readonly DateTime LastActivity;
        [JsonProperty("closed")] public readonly bool Closed;
        [JsonProperty("actions")] public readonly TrelloCardAction[] Actions;

        public IEnumerable<TrelloCardAction> GetActions(TrelloBoard board) {
            return Actions ?? board.Actions.Where(x => x.Data.Card?.ShortLink == ShortLink);
        }

        public bool Processed;
        public override string ToString() {
            return Name ?? Url;
        }
    }
    [JsonObject(MemberSerialization.Fields)]
    public class TrelloCardAction {
        [JsonProperty("data")] public readonly TrelloCardActionData Data;
        [JsonProperty("memberCreator")] public readonly TrelloMemberCreator Member;
        [JsonProperty("type")] public readonly string Type;
        [JsonProperty("date")] public readonly DateTime Date;
        public override string ToString() {
            return Type;
        }
    }

    [JsonObject(MemberSerialization.Fields)]
    public class TrelloCardActionData {
        [JsonProperty("card")] public readonly TrelloCard Card;
        [JsonProperty("list")] public readonly TrelloList List;
        [JsonProperty("listBefore")] public readonly TrelloList ListBefore;
        [JsonProperty("listAfter")] public readonly TrelloList ListAfter;
    }

    [JsonObject(MemberSerialization.Fields)]
    public class TrelloMemberCreator {
        [JsonProperty("fullName")] public readonly string FullName;
        [JsonProperty("initials")] public readonly string Initials;
    }

    [JsonObject(MemberSerialization.Fields)]
    public class TrelloList {
        [JsonProperty("id")] public readonly string ID;
        [JsonProperty("name")] public readonly string Name;
        [JsonProperty("closed")] public readonly bool Closed;
    }

    [JsonObject(MemberSerialization.Fields)]
    public class TrelloBoard {
        [JsonProperty("name")] public readonly string Name;
        [JsonProperty("cards")] public readonly TrelloCard[] Cards;
        [JsonProperty("lists")] public readonly TrelloList[] Lists;
        [JsonProperty("closed")] public readonly bool Closed;
        [JsonProperty("actions")] public readonly TrelloCardAction[] Actions;

        public IEnumerable<TrelloCard> GetCards(string listName, bool? closed = null) {
            var list = Lists.FirstOrDefault(x => x.Closed == false && x.Name == listName);
            return Cards.Where(x => x.ListID == list.ID && closed.HasValue && x.Closed == closed.Value);
        }
    }
#pragma warning restore 0169, 0649

    public class TrelloBacklog {
        public readonly List<TrelloCard> Cards = new List<TrelloCard>();
        public TrelloCard FindCard(BacklogTask task) {
            return Cards.FirstOrDefault(c => {
                var shortLink = task.ShortLink;
                if (shortLink != null && c.ShortLink == shortLink)
                    return true;
                
                var url = task.URL;
                if (url != null && url.Contains(c.ShortLink, StringComparison.InvariantCulture))
                    return true;

                return false;
            });
        }
        private void AddCards(Board board) {
            var jsonStr = File.ReadAllText(board.BoardJsonPath);
            var trelloBoard = JsonConvert.DeserializeObject<TrelloBoard>(jsonStr);
            foreach (var listName in board.Lists)
                Cards.AddRange(trelloBoard.GetCards(listName, false));
        }

        public static TrelloBacklog Load(Board[] boards) {
            var result = new TrelloBacklog();
            foreach (var board in boards) {
                result.AddCards(board);
            }
            return result;
        }
        public static TrelloBacklog Load(Board board) {
            var result = new TrelloBacklog();
            result.AddCards(board);
            return result;
        }
    }
}
