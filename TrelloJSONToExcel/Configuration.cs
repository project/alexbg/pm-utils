﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TrelloJSONToExcel { 
#pragma warning disable 0169, 0649
    [JsonObject(MemberSerialization.Fields)]
    public class Configuration {
        [JsonProperty("relations")] public readonly Relation[] Relations;
        [JsonProperty("lastSyncDate")] public readonly DateTime LastSyncDate;

        public static Configuration Load(string path) {
            var jsonStr = File.ReadAllText(path);
            var configuration= JsonConvert.DeserializeObject<Configuration>(jsonStr);
            return configuration;
        } 
    }
    [JsonObject(MemberSerialization.Fields)]
    public class Relation {
        [JsonProperty("backlogName")] public readonly string BacklogName;
        [JsonProperty("boards")] public readonly Board[] Boards;
    }
    [JsonObject(MemberSerialization.Fields)]
    public class Board {
        [JsonProperty("boardJson")] public readonly string BoardJsonPath;
        [JsonProperty("lists")] public readonly string[] Lists;
        public Board(string boardJsonPath, string[] lists) {
            BoardJsonPath = boardJsonPath;
            Lists = lists;
        }
    }
#pragma warning restore 0169, 0649
}
