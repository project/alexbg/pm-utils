﻿using DevExpress.Spreadsheet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TrelloJSONToExcel {
    class Program {
        static void Main(string[] args) {
            var excelBacklogPath = args[0];
            var configPath = args[1];
            var configuration = Configuration.Load(configPath);

            using (var workbook = new Workbook()) {
                workbook.LoadDocument(excelBacklogPath);
                foreach (var relation in configuration.Relations) {
                    var excelBacklog = ExcelBacklog.Load(workbook, relation.BacklogName);                    
                    var trelloBacklog = TrelloBacklog.Load(relation.Boards);
                    var mergedBacklog = ExcelBacklog.Merge(excelBacklog, trelloBacklog, configuration.LastSyncDate);
                    mergedBacklog.Upload($"MergedBacklog {DateTime.Now.ToString("yyyy-MM-dd")}.xlsx", relation.BacklogName);
                }
            }
        }
    }
}
