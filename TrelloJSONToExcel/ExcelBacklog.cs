﻿using DevExpress.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace TrelloJSONToExcel {
    public class TaskPropertyValue {
        public readonly string Text;
        public readonly string URL;
        public TaskPropertyValue(string text, string url = null) {
            Text = text;
            URL = url;
        }
        public override string ToString() {
            if (string.IsNullOrEmpty(URL))
                return string.IsNullOrEmpty(Text) ? "-": Text;
            return $"<{Text} : \"{URL}\">";
        }
    }
    public enum ChangeKind {
        None,
        Added,
        Deleted,
        Updated
    }
    public class BacklogTask : Dictionary<string, TaskPropertyValue> {
        public const string SHORT_LINK = "Short Link";
        public const string LAST_ACTIVITY = "Last Activity";
        public const string TASK = "Task";

        public readonly ChangeKind ChangeKind = ChangeKind.None;
        public BacklogTask() { }
        public BacklogTask(TrelloCard card, ChangeKind changeKind) {
            ChangeKind = changeKind;
            UpdateWithCard(card);
        }
        BacklogTask(BacklogTask source, ChangeKind changeKind) {
            ChangeKind = changeKind;
            foreach (var pair in source)
                this[pair.Key] = pair.Value;
        }
        void UpdateWithCard(TrelloCard card) {
            if (card == null)
                return;

            this[TASK] = new TaskPropertyValue(card.Name, card.Url);
            this[LAST_ACTIVITY] = new TaskPropertyValue(card.LastActivity.ToString("yyyy-MM-dd"));
            this[SHORT_LINK] = new TaskPropertyValue(card.ShortLink);
        }
        public string URL => this.TryGetValue(TASK, out var val) ? val.URL : null;
        public string ShortLink => this.TryGetValue(SHORT_LINK, out var val) ? val.Text: null;
        public DateTime? LastActivity {
            get {
                var strDate = this.TryGetValue(LAST_ACTIVITY, out var val) ? val.Text : null;
                if (DateTime.TryParse(strDate, out var result))
                    return result;
                return null;
            }
        }
        public Color FillColor {
            get {
                switch (ChangeKind) {
                    case ChangeKind.Added: 
                        return Color.PaleGreen;
                    case ChangeKind.Deleted:
                        return Color.LightPink;
                    case ChangeKind.Updated:
                        return Color.LightYellow;        
                }
                return Color.Transparent;
            }
        } 
        public BacklogTask WithChange(TrelloCard card, ChangeKind changeKind) {
            var result = new BacklogTask(this, changeKind);
            result.UpdateWithCard(card);
            return result;
        }
    }

    public class ExcelBacklog {
        string[] columnNames;
        public readonly List<BacklogTask> Tasks;

        ExcelBacklog(List<BacklogTask> tasks) {
            this.Tasks = tasks;
            UpdateColumnNames();
        }
        void UpdateColumnNames() {
            var set = new HashSet<string>();
            foreach (var task in Tasks) {
                foreach (var pair in task)
                    set.Add(pair.Key);
            }
            columnNames = set.ToArray();
        }
        static bool CheckTaskUpdated(BacklogTask task, TrelloCard card, DateTime? lastSyncDate) {
            var taskLastActivity = task.LastActivity;
            var cardLastActivity = card.LastActivity.Date;
            if (taskLastActivity == null && lastSyncDate == null)
                return false;

            if (lastSyncDate.HasValue)
                return cardLastActivity > lastSyncDate.Value.Date;

            if (taskLastActivity.HasValue)
                return cardLastActivity > taskLastActivity.Value.Date;

            return false;
        }
        public int Count => Tasks.Count;
        public int CountAdded => Tasks.Count(t => t.ChangeKind == ChangeKind.Added);
        public int CountUpdated => Tasks.Count(t => t.ChangeKind == ChangeKind.Updated);
        public int CountDeleted => Tasks.Count(t => t.ChangeKind == ChangeKind.Deleted);
        public int CountPreserved => Tasks.Count(t => t.ChangeKind == ChangeKind.None);

        public BacklogTask FindTask(TrelloCard card) {
            return Tasks.FirstOrDefault(t => {
                var shortLink = t.ShortLink;                
                if (shortLink != null && shortLink == card.ShortLink)
                    return true;

                var url = t.URL;
                if (url != null && url.Contains(card.ShortLink, StringComparison.InvariantCulture))
                    return true;

                return false;
            });
        }
        public void Upload(string path, string sheetName) {
            using (var workbook = new Workbook()) {
                if(File.Exists(path))
                    workbook.LoadDocument(path);

                try {
                    workbook.BeginUpdate();
                    Worksheet sheet = workbook.Worksheets.Contains(sheetName)
                        ? workbook.Worksheets[sheetName]
                        : workbook.Worksheets.Add(sheetName);

                    sheet.Clear(sheet.GetDataRange());
                    var range = sheet["1:1"];
                    for (int i = 0; i < columnNames.Length; i++)
                        range[i].Value = columnNames[i];

                    foreach (var task in Tasks) {
                        range = range.Offset(1, 0);
                        for (int col = 0; col < columnNames.Length; col++) {
                            var cell = range[col];
                            cell.FillColor = task.FillColor;
							
                            if (!task.TryGetValue(columnNames[col], out var value))
                                continue;

                            if (string.IsNullOrEmpty(value.URL)) {
                                if (int.TryParse(value.Text, out var intValue))
                                    cell.Value = intValue;
                                else
                                    cell.Value = value.Text;
                            } else {
                                sheet.Hyperlinks.Add(cell, value.URL, true, value.Text);
                            }
                        }
                    }
                } finally {
                    workbook.EndUpdate();
                    workbook.SaveDocument(path);
                }                
            }
        }

        public static ExcelBacklog Load(Workbook workbook, string name) {
            var sheet = workbook.Worksheets[name];
            var table = sheet.Tables.Single();
            var tasks = new List<BacklogTask>();

            var columnNames = table.Columns.Select(c => c.Name).ToArray();
            var dataRange = table.DataRange;
            for (int r = 0; r < dataRange.RowCount; r++) {
                var task = new BacklogTask();
                for (int c = 0; c < dataRange.ColumnCount; c++) {
                    var cell = dataRange[r, c];
                    var hyperLinks = sheet.Hyperlinks.GetHyperlinks(cell);
                    var cellValue = cell.Value.ToString();
                    if (hyperLinks.Count == 0) 
                        task[columnNames[c]] = new TaskPropertyValue(cellValue);
                    else {
                        var link = hyperLinks.First();
                        task[columnNames[c]] = new TaskPropertyValue(cellValue, link.Uri);
                    }
                }

                tasks.Add(task);
            }

            return new ExcelBacklog(tasks);
        }
        public static ExcelBacklog Merge(ExcelBacklog excelBacklog, TrelloBacklog trelloBacklog, DateTime? lastSyncDate = null) {
            var mergedTasks = new List<BacklogTask>();
            foreach (var task in excelBacklog.Tasks) {
                var card = trelloBacklog.FindCard(task);
                if (card == null) {
                    mergedTasks.Add(task.WithChange(null, ChangeKind.Deleted));
                    continue;
                }

                if (CheckTaskUpdated(task, card, lastSyncDate))                    
                    mergedTasks.Add(task.WithChange(card, ChangeKind.Updated));
                else
                    mergedTasks.Add(task.WithChange(card, ChangeKind.None));

                card.Processed = true;
             }

            mergedTasks.AddRange(
                trelloBacklog.Cards
                    .Where(c => !c.Processed)
                    .Select(c => new BacklogTask(c, ChangeKind.Added))
            );

            return new ExcelBacklog(mergedTasks);
        }
    }
}
