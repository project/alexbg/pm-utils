﻿using DevExpress.Spreadsheet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrelloJSONToExcel;

namespace BugFixTime {
    static class Extensions {
        const string UPDATE_CARD = "updateCard";
        const string CREATE_CARD = "createCard";
        const string INCOMING = "Incoming";
        const string DOING = "Doing";
        const string SPRINT = "Sprint";

        public static IEnumerable<TrelloCard> GetBugs(this TrelloBoard board) {
            var createBugActions = board.Actions.Where(x => x.Type == CREATE_CARD && x.Data.List.Name == INCOMING); 
            foreach(var action in createBugActions) {
                var card = board.Cards.First(x => x.ShortLink == action.Data.Card.ShortLink);
                yield return card;
            }
        }
        public static bool TryGetFixDate(this TrelloBoard board, TrelloCard card, out List<DateTimeInterval> doingIntervals, out string author) {
            doingIntervals = new List<DateTimeInterval>();
            author = null;
            try {
                var actions = card.GetActions(board).ToList();
                actions.Sort((x, y) => {
                    if (x.Date < y.Date)
                        return -1;
                    if (x.Date > y.Date)
                        return 1;
                    return 0;
                });

                bool done = false;
                for (int i = 0; i < actions.Count; i++) {
                    var action = actions[i];
                    if (action.Type == UPDATE_CARD && action.Data.ListAfter != null) {
                        if (action.Data.ListAfter.Name == DOING)
                            doingIntervals.Add(new DateTimeInterval() { Start = action.Date });
                        else {
                            var interval = doingIntervals.LastOrDefault();
                            if (interval != null && !interval.IsFinished)
                                interval.End = action.Date;
                        }

                        done = action.Data.ListAfter.Name.StartsWith(SPRINT);
                        if(done)
                            author = action.Member.FullName;
                    }
                }

                if(done && doingIntervals.Count > 0 && doingIntervals.Last().IsFinished)
                    return true;

                doingIntervals = null;
                return false;
            } catch {        
                return false;
            }
        }
        public static IEnumerable<BugFixTime> GetBugFixTime(this TrelloBoard board, IEnumerable<TrelloCard> bugs) {
            foreach (var bug in bugs) {
                if (board.TryGetFixDate(bug, out var doingIntervals, out var author))
                    yield return new BugFixTime(bug, author, doingIntervals);
            }
        }
    }

    public class DateTimeInterval {
        public DateTime Start = DateTime.MinValue;
        public DateTime End = DateTime.MinValue;
        public double Hours => (End - Start).TotalHours;
        public bool IsFinished => End != DateTime.MinValue;
    }

    class BugFixTime {
        public readonly TrelloCard Bug;
        public readonly DateTime StartDate;
        public readonly DateTime EndDate;
        public readonly TimeSpan Interval;
        public double Hours => Interval.TotalHours;
        public double Days => Interval.TotalDays;
        public readonly double CorrectHours;
        public readonly double CorrectWorkingDays;
        public readonly string Author;

        double GetWorkingHours(DateTimeInterval doingInterval) {
            if (doingInterval.Start.Date.Day == doingInterval.End.Date.Day) // single day
                return doingInterval.Hours;

            // different days
            var wdStart = doingInterval.Start.Date.AddHours(9); // start of the first working day 
            var wdEnd = doingInterval.Start.Date.AddHours(18); // end of the first working day 

            var result = (wdEnd - doingInterval.Start).TotalHours;
            for (var i = wdStart.AddDays(1); i < doingInterval.End.Date; i = i.AddDays(1)) {
                if (i.DayOfWeek == DayOfWeek.Saturday || i.DayOfWeek == DayOfWeek.Sunday)
                    continue;
                result += 8;
            }

            var endHours = (doingInterval.End - doingInterval.End.Date.AddHours(9)).TotalHours;
            result += endHours > 0 ? endHours: 0;
            
            return result;
        }

        public BugFixTime(TrelloCard bug, string author, List<DateTimeInterval> doingIntervals) {
            Bug = bug;
            Author = author;
            StartDate = doingIntervals.Select(x => x.Start).Min();
            EndDate = doingIntervals.Select(x => x.End).Max(); ;

            Interval = EndDate - StartDate;

            CorrectHours = 0;
            foreach (var di in doingIntervals)
                CorrectHours += GetWorkingHours(di);

            CorrectWorkingDays = CorrectHours / 8;
        }
    }

    class Program {
        static void Main(string[] args) {
            var trelloJsonPath = args[0];
            var directory = Path.GetDirectoryName(trelloJsonPath);
            var fileName = Path.GetFileNameWithoutExtension(trelloJsonPath);
            var excelOutputPath = Path.Combine(directory, fileName + ".xlsx");

            var jsonStr = File.ReadAllText(trelloJsonPath);
            var trelloBoard = JsonConvert.DeserializeObject<TrelloBoard>(jsonStr);

            var bugs = trelloBoard.GetBugs();
            var bugFixTime = trelloBoard.GetBugFixTime(bugs);

            using (var workbook = new Workbook()) {
                try {
                    workbook.BeginUpdate();

                    var  sheet = workbook.Worksheets.ActiveWorksheet;
                    sheet.Clear(sheet.GetDataRange());

                    var range = sheet["1:1"];
                    range[0].Value = "Card Name";
                    range[1].Value = "Author";
                    range[2].Value = "Start";
                    range[3].Value = "End";
                    range[4].Value = "Hours";
                    range[5].Value = "Days";
                    range[6].Value = "CorrectHours";
                    range[7].Value = "CorrectWorkingDays";

                    foreach (var time in bugFixTime) {
                        range = range.Offset(1, 0);

                        sheet.Hyperlinks.Add(range[0], time.Bug.Url, true, time.Bug.Name);
                        range[1].Value = time.Author;
                        range[2].Value = time.StartDate;
                        range[3].Value = time.EndDate;
                        range[4].Value = time.Hours;
                        range[5].Value = time.Days;
                        range[6].Value = time.CorrectHours;
                        range[7].Value = time.CorrectWorkingDays;
                    }
                } finally {
                    workbook.EndUpdate();
                    workbook.SaveDocument(excelOutputPath);
                }
            }
        }
    }
}
