﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ParseLicOwners {
    struct UserRange {
        public static UserRange Empty = new UserRange(-1, -1);
        UserRange(int start, int end) {
            Start = start;
            End = end;
        }
        public readonly int Start;
        public readonly int End;
        public bool IsEmpty => Start < 0 && End < 0;

        public UserRange WithStart(int start) {
            return new UserRange(start, End);
        }
        public UserRange WithEnd(int end) {
            return new UserRange(Start, end);
        }
    }
    struct LicenseInfo {
        int? totalLicenses;
        // countries
        public Dictionary<string, int> Subscriptions;

        public string[] ActiveUsers;
        public readonly string Id;

        public LicenseInfo(string id) {
            Id = id;
            totalLicenses = null;
            Subscriptions = null;
            ActiveUsers = null;
        }
        public int GetTotalLicenses() {
            if (totalLicenses.HasValue)
                return totalLicenses.Value;

            return (totalLicenses = Subscriptions?.Sum(p => p.Value)).GetValueOrDefault();
        }
        public void AddSubscription(string name, int count) {
            if (Subscriptions == null)
                Subscriptions = new Dictionary<string, int>();
            
            if (Subscriptions.ContainsKey(name))
                Subscriptions[name] += count;
            else
                Subscriptions.Add(name, count);
        }
        public override string ToString() {
            return $"{Id}:{GetTotalLicenses()}";
        }
    }
    class Program {
        const string ID = "Id";
        const string SUBSCRIPTION = "Subscription";
        const string ACTIVE_USERS = "Active users";
        const string PRIORITY_SUPPORT = "(Priority Support)";


        static IEnumerable<UserRange> GetUserRanges(string[] lines) {
            UserRange result = UserRange.Empty;
            for (int i = 0; i < lines.Length; i++) {
                var line = lines[i];
                if (line.StartsWith(ID)) {
                    if (!result.IsEmpty)
                        yield return result.WithEnd(i - 1);

                    result = UserRange.Empty.WithStart(i);
                }
            }

            if (!result.IsEmpty)
                yield return result.WithEnd(lines.Length - 1);
        }
        static string GetID(string line) {
            if (!line.StartsWith(ID + ":"))
                throw new Exception();

            return line.Substring(3).Trim();
        }
        static LicenseInfo ParseUserRange(UserRange range, string[] lines) {
            var id = GetID(lines[range.Start]);
            var result = new LicenseInfo(id);

            ParseSubscriptions(range, lines, ref result);
            ParseActveUsers(range, lines, ref result);
            return result;
        }
        static void ParseSubscriptions(UserRange range, string[] lines, ref LicenseInfo result) {
            for (int i = range.Start + 1; i <= range.End; i++) {
                var line = lines[i];
                var sStart = line.IndexOf(SUBSCRIPTION);
                if (sStart < 0)
                    continue;

                var sName = line.Substring(0, sStart).Trim();
                var startIndex = line.IndexOf(":");
                var sCount = line.Substring(startIndex + 1).Trim();
                if (!int.TryParse(sCount, out var count))
                    throw new Exception();

                result.AddSubscription(sName, count);
            }
        }
        static void ParseActveUsers(UserRange range, string[] lines, ref LicenseInfo result) {
            for (int i = range.Start + 1; i <= range.End; i++) {
                var line = lines[i];
                if (!line.StartsWith(ACTIVE_USERS))
                    continue;

                line = line.Substring(ACTIVE_USERS.Length + 1).Trim();
                var activeUsers = line.Split(',').Select(s => s.Trim()).ToArray();
                result.ActiveUsers = activeUsers;
                break;
            }
        }

        static Dictionary<string, LicenseInfo> processedLicenseInfo = new Dictionary<string, LicenseInfo>();
        static Dictionary<string, int> ownersCounts = new Dictionary<string, int>();
        static Dictionary<string, int> licenseCounts = new Dictionary<string, int>();
        static int TotalAccumulatedLicenseOwners = 0;
        static int TotalAccumulatedLicenseCount = 0;

        static bool CheckFullDuplicateSafe(LicenseInfo current, LicenseInfo processed, int step) {
            try {
                ThrowIfNotFullDuplicate(current, processed);
                return true;
            } catch(Exception ex) {
                Console.WriteLine($"{step} => {current} & {processed} => {ex.Message}");
                return false;
            };
        }
        static void ThrowIfNotFullDuplicate(LicenseInfo current, LicenseInfo processed) {
            if (current.Subscriptions.Count != processed.Subscriptions.Count)
                throw new Exception($"Count {current.Subscriptions.Count} != {processed.Subscriptions.Count}");

            var currentPairs = current.Subscriptions.ToArray();
            for (int i = 0; i < currentPairs.Length; i++) {
                var pair = currentPairs[i];
                var processedValue = processed.Subscriptions[pair.Key];
                if (pair.Value != processedValue)
                    throw new Exception("Subscriptions: " + pair.ToString());
            }

            for (int i = 0; i < current.ActiveUsers.Length; i++) {
                var u = current.ActiveUsers[i];
                if (!processed.ActiveUsers.Contains(u))
                    throw new Exception("ActiveUser: " + u);
            }
        }
        static bool CanAccumulate(LicenseInfo licInfo) {
            if(processedLicenseInfo.TryGetValue(licInfo.Id, out var processed)) {
                CheckFullDuplicateSafe(licInfo, processed, 0);
                return false; // 100%
            }

            foreach (var u in licInfo.ActiveUsers) {
                if (processedLicenseInfo.TryGetValue(u, out var cached)) {
                    CheckFullDuplicateSafe(licInfo, cached, 1);
                    continue;
                }

                processedLicenseInfo.Add(u, licInfo);
            }
            return true;
        }
        static void Accumulate(LicenseInfo licInfo) {
            if (licInfo.Subscriptions == null)
                return;

            if (!CanAccumulate(licInfo)) {
                Console.WriteLine($"Haven't been accumulated: {licInfo}");
                return;
            }

            TotalAccumulatedLicenseOwners++;
            TotalAccumulatedLicenseCount += licInfo.GetTotalLicenses();
            foreach (var pair in licInfo.Subscriptions) {
                if (ownersCounts.ContainsKey(pair.Key))
                    ownersCounts[pair.Key]++;
                else
                    ownersCounts.Add(pair.Key, 1);

                if (licenseCounts.ContainsKey(pair.Key))
                    licenseCounts[pair.Key]+= pair.Value;
                else
                    licenseCounts.Add(pair.Key, pair.Value);
            }
        }
        static void OutputAccumulatedResults(string outputPath) {
            using(var file = File.OpenWrite(outputPath)) 
            using(var writer = new StreamWriter(file)) {
                writer.WriteLine($"## License Onwers, TOTAL: {TotalAccumulatedLicenseOwners}");
                foreach(var pair in ownersCounts)
                    writer.WriteLine($"{pair.Key}: {pair.Value}");

                writer.WriteLine();
                writer.WriteLine($"## License Count, TOTAL: {TotalAccumulatedLicenseCount}");
                foreach (var pair in licenseCounts)
                    writer.WriteLine($"{pair.Key}: {pair.Value}");
            }
        }
        static IEnumerable<LicenseInfo> GetLicenceInfos(string[] filePaths) {
            foreach (var fileName in filePaths) {
                var lines = File.ReadAllLines(fileName);
                var userRanges = GetUserRanges(lines).ToArray();
                foreach (var range in userRanges)
                    yield return ParseUserRange(range, lines);
            }
        }
        static void Main(string[] args) {
            var licInfoList = GetLicenceInfos(args).ToList();
            licInfoList.Sort((x, y) => {
                var tx = x.GetTotalLicenses();
                var ty = y.GetTotalLicenses();

                if (tx < ty)
                    return 1;
                if (tx > ty)
                    return -1;
                return 0;
            });

            foreach(var licInfo in licInfoList)
                Accumulate(licInfo);

            OutputAccumulatedResults("output.txt");
        }
    }
}
